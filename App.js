import React, { useState } from 'react';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Controller from './controller/Controller';
import ComplaintForm from './components/ComplaintForm';
import Login from './authentication/Login';
import Adminlogin from './authentication/Adminlogin';
import { Button, FlatList, Image, StatusBar, StyleSheet, TouchableOpacity,Dimensions, Linking } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; // Importing Ionicons from Expo
import { Text, View } from 'react-native';
import PublicRaiseComplaint from './components/publicRaiseComplaint/PublicRaiseComplaint';
import ExciseOfficerForm from './components/eoForm/ExciseOfficerForm';
import Dashboard from './components/Dashboard';
import DCdashboard from './components/dashboards/DCdashboard';
import DcForm from './components/dcform/DCForm';
import AboutUs from './components/about/AboutUs';
import Entypo from 'react-native-vector-icons/Entypo';
import EHOdashboard from './components/dashboards/EHOdashboard';
import Agentdashboard from './components/dashboards/Agentdashboard';
import Agenthome from './components/dashboards/Agenthome';
import Tracker from './Tracker/Tracker';
// Create Drawer and Stack navigators
const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();

// Define Stack Navigator
function AppStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Dashboard"
        component={Dashboard}
        options={{
          headerShown: false,
          title: 'Home'
        }}
      />
      
      <Stack.Screen name='PublicRaiseComplaint' component={PublicRaiseComplaint} options={{headerShown:false}}/>
      <Stack.Screen name='DCdashboard' component={DCdashboard} options={{headerShown:false}} />
      <Stack.Screen name='AboutUs' component={AboutUs} options={{headerShown:false}} />
      <Stack.Screen name='EHO' component={EHOdashboard} options={{headerShown:false}} />
      <Stack.Screen name='Controller' component={Controller} options={{headerShown:false}} />
      <Stack.Screen name='agenthome' component={Agenthome} options={{headerShown:false}} />
      <Stack.Screen name='Agentdashboard' component={Agentdashboard} options={{headerShown:false}} />
    </Stack.Navigator>
  );
}


const { width, height } = Dimensions.get('window');

const CustomHeaderLeft = () => {
  const navigation = useNavigation();
  const [loginClick, setLoginClick] = useState(false);

  const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      marginLeft: 0,
      paddingVertical: height * 0.02,
      justifyContent: 'space-evenly',
      width: width,
      height:width*0.27,
    },
    image: {
      width: width * 0.23,
      height: height * 0.24,
      resizeMode: 'contain',
      marginLeft: width * 0.02,
    },
    titleContainer: {
      flex: 1,
      marginLeft: width * 0.01,
    },
    title: {
      fontSize: width * 0.05,
      color: 'green',
      fontWeight: 'bold',
      flexWrap: 'wrap',
    },
    title1: {
      fontSize: width * 0.03,
      fontWeight: 'bold',
    },
    container1: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: 'green',
      paddingVertical: height * 0.01,
      width: width,
    },
    list: {
      position: 'absolute',
      top: height * 0.05,
      // borderColor: 'lightgray',
      // borderWidth: 0.5,
      height: height * 0.12,
      width: width * 0.2,
      borderRadius: 10,
      // backgroundColor: 'white',
    },
    btn: {
      backgroundColor: 'green',
      height: height * 0.05,
      marginBottom: 2,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 10,
      
    },
    touchLogin: {
      fontSize: width * 0.04,
      backgroundColor: 'white',
      color: 'green',
      paddingHorizontal: width * 0.03,
      paddingVertical: height * 0.005,
      fontWeight: 'bold',
      borderRadius: 5,
    },
    mailpto: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    mailText: {
      color: 'white',
      paddingLeft: width * 0.02,
      fontSize: width * 0.03,
    },
    menuIcon: {
      marginRight: width * 0.04,
    },
  });

  return (
    <View>
      <StatusBar />
      <View style={styles.container}>
        <Image source={require('./assets/logo/apLogo1.png')} style={styles.image} />
        <View style={styles.titleContainer}>
          <Text style={styles.title}>ANDHRAPRADESH</Text>
          <Text style={styles.title1}>PROHIBITION & EXCISE DEPARTMENT</Text>
        </View>
        <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
          <Ionicons name="menu" size={width * 0.08} color="#000" style={styles.menuIcon} />
        </TouchableOpacity>
      </View>
      <View style={styles.container1}>
        <View style={styles.mailpto}>
       
          <Entypo name='mail' size={width * 0.05} style={{ color: 'white' }} />
          <Text style={styles.mailText} onPress={()=>{Linking.openURL('mailto:ap.excise@gmail.com')}}>ap.excise@gmail.com</Text>
          
        </View>
        <View>
          <TouchableOpacity onPress={() => setLoginClick(!loginClick)}>
            <Text style={styles.touchLogin}>Log In</Text>
          </TouchableOpacity>
          {loginClick && (
            <FlatList
              style={styles.list}
              data={[{ navigate: 'Login', title: 'Public' }, { navigate: 'Adminlogin', title: 'Employee' }]}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.btn}
                  onPress={() => {
                    navigation.navigate(item.navigate);
                    setLoginClick(false);
                  }}
                >
                  <Text style={{color:'white', fontWeight:'700'}}>{item.title}</Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.navigate}
            />
          )}
        </View>
      </View>
    </View>
  );
};


// Define Drawer Navigator
function MyDrawer() {
  return (
    <Drawer.Navigator initialRouteName="Home" screenOptions={{
      // headerRight:({navigation,route})=>{return <HeaderRight />},
      header: () => <CustomHeaderLeft />,

      headerTitle: () => { return null },
      headerStyle: { height: 150 }
    }}>
      <Drawer.Screen name="Home" component={AppStack} />

      <Drawer.Screen name="PublicRaiseComplaint" component={PublicRaiseComplaint} options={{
        title: 'Public Complaint Form'
      }} />
      <Drawer.Screen name="EoForm" component={ExciseOfficerForm} options={{
        title: 'Enquiry Officer Form'
      }} />
      <Drawer.Screen name="DCdashboard" component={DCdashboard} options={{
          title:'DC dashboard'
        }}/>
        <Drawer.Screen name="Agentdashboard" component={Agentdashboard} options={{
          title:'Agent dashboard'
        }}/>
      <Drawer.Screen name="ComplaintForm" component={ComplaintForm} options={{
        title: 'Agent Complaint Form'
      }} />
      <Drawer.Screen name="DCForm" component={DcForm} options={{
        title: 'DC Form'
      }} />
      <Drawer.Screen name="AboutUs" component={AboutUs} options={{
        title: 'About Us'
      }} />
      <Drawer.Screen name="Controller" component={Controller} options={{
        title: 'Controller'
      }} />
      <Drawer.Screen name="Login" component={Login} options={{
        title: 'Login'
      }} />
      <Drawer.Screen name="Adminlogin" component={Adminlogin} options={{
        title: 'Admin Login'
      }} />
      <Drawer.Screen name="tracker" component={Tracker} options={{
        title: 'Tracker'
      }} />
    </Drawer.Navigator>
  );
}

// Main App Component
const App = () => {
  return (
    <NavigationContainer>
      <MyDrawer />
    </NavigationContainer>
  );
}

export default App;
