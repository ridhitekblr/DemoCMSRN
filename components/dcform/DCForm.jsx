import { StatusBar,  TextInput, View,Button,Text, ScrollView, TouchableOpacity,StyleSheet,Image,Linking,Alert,Modal,Pressable} from 'react-native'

import React from 'react'
import { Dropdown } from 'react-native-element-dropdown';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useState ,useEffect} from 'react';
import * as DocumentPicker from 'expo-document-picker';
import DropDownPicker from 'react-native-dropdown-picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ATRpreliminary from './ATRpreliminary';
const DcForm = () => {
  const [valueo, setValueo] = useState(null);
  const assignedtodata = [
    { label: 'SHO', value: 'option1' },
    { label: 'ESTF', value: 'option2' },
    { label: 'ENF', value: 'option3' },
    { label: 'BMPP', value: 'option4' },
    { label: 'CP', value: 'option5' },
    { label: 'SELF', value: 'option6' },
   
  ];
  const crimeDetection= [
    { label: 'Detected', value: 'option1' },
    { label: 'Not Detected', value: 'option2' },
  ];
  const seizureType= [
    { label: 'vehicles', value: 'option1' },
    { label: 'Contraband', value: 'option2' },
    { label: 'Others', value: 'option3' },
  ];
  const propertyType= [
    { label: '', value: 'option1' },
    { label: '', value: 'option2' },
  ];
  const illicitLiquor= [
    { label: ' Adulterated Toddy', value: 'option1' },
    { label: 'NDPL', value: 'option2' },
    { label: ' Duty Paid Liquor (IML) ', value: 'option3' },
    { label: ' Spurious Liquor', value: 'option4' },
    { label: ' RS/ENA', value: 'option5' }, 
    { label: ' ID Liquor ', value: 'option6' },
    { label: ' Wash', value: 'option7' },
    { label: ' Others(IL)', value: 'option8' }, 
  ];
  const [hover, setHover] = useState(false);
  const statusData = [
    { label: 'New(C)', value: 'option1' },
    { label: 'Assigned To Enquiry Officer', value: 'option2' },
    { label: 'ATR Preliminary Submitted', value: 'option3' },
    { label: 'Review', value: 'option4' },
    { label: 'Enquiry Completed', value: 'option5' },
   
  ];
  const [complaintId, setComplaintId] = useState('SKL1802190001');
  const [complaintDetails, setComplaintDetails] = useState('Sample complaint details picked from Public Complaint form');
  const [complaintType, setComplaintType] = useState('Sample complaint type picked from Control Room Form');
  const [attachments, setAttachments] = useState('Sample attachment picked from Public Complaint form');
  const [mandal, setMandal] = useState('Sample mandal picked from Public Complaint form');
  const [village, setVillage] = useState('Sample village picked from Public Complaint form');
  
    const validationSchema = Yup.object().shape({
     
        statusData: Yup.string().required('DropDown field  is required'),
        
        assignedto:Yup.string().required("DropDown field is required"),
        
        Finalremarks: Yup.string().min(10, 'Complaint Source must be at least 6 characters').required('Final Remarks required'),
        
      });    
      const [timeStamp, setTimeStamp] = useState('');

  useEffect(() => {
    const currentDateTime = new Date().toLocaleString();
    setTimeStamp(currentDateTime);
  }, []);
  
  const [selectedFile, setSelectedFile] = useState(null);

  const handleFilePick = async (setFieldValue) => {
    try {
      let result = await DocumentPicker.getDocumentAsync({
        type: ['image/*', 'application/pdf'],
        copyToCacheDirectory: true,
      });
      console.log('result',result);
      if(!result.success){
        const assets = result.assets;
        const name = assets[0].name
        setFieldValue('selectedFile', name)
      }
   
    } catch (error) {
      Alert.alert('Error', 'An error occurred while picking the file');
      console.error('File pick error:', error);
    }
  };
      const openLink = (url) => {
        Linking.openURL(url).catch((err) => console.error("Failed to open URL:", err));
      };
      const [modalVisible, setModalVisible] = useState(false);

  return (
    <ScrollView style={styles.cont} showsVerticalScrollIndicator={false} >
    <View>
        <StatusBar></StatusBar>
      
        <View style={styles.cont}>
        <Text style={styles.header}>DC FORM</Text>
      
              <View style={styles.container}>
              
              <Formik
              initialValues={{ 
                statusData:'', assignedto:'',
                Finalremarks:'',
              }}
              validationSchema={validationSchema}
              onSubmit={(values) => {
                Alert.alert("Success", "Form submitted successfully!",);
                
              }}
            >
              {({ handleChange, handleBlur, handleSubmit, setFieldTouched,setFieldValue, values, errors, touched }) => (
                <View>
  <View style={styles.fieldContainer}>
        <Text style={styles.label}>Complaint Id</Text>
        <TextInput style={styles.input2} value={complaintId} editable={false} />
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.label}>Complaint Details</Text>
        <TextInput style={styles.input2} value={complaintDetails} editable={false} />
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.label}>Complaint Type</Text>
        <TextInput style={styles.input2} value={complaintType} editable={false} />
      </View>

      <View style={styles.fieldContainer}>
        <Text style={styles.label}> Attachment</Text>
        <TextInput style={styles.input2} value={attachments} editable={false} />
        <TouchableOpacity onPress={() => handleFilePick(setFieldValue)} style={styles.fileButton}>
        <Text style={styles.fileButtonText} >Download Attachment</Text>
      </TouchableOpacity>
      <Text>{values.selectedFile}</Text> 
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.label}>Mandal</Text>
        <TextInput style={styles.input2} value={mandal} editable={false} />
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.label}>Village</Text>
        <TextInput style={styles.input2} value={village} editable={false} />
      </View>
                    <Text style={styles.text}>Timestamp:</Text>
              <TextInput
                style={styles.input}
                value={timeStamp}
                editable={true}
              />
              <Text style={styles.text}>Status</Text>
           
              <Dropdown
                    style={styles.input}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    data={statusData}
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!hover ? 'Select status Data' : '...'}
                    value={values.statusData}
                    onFocus={() => setHover(true)}
                    onBlur={() => setHover(false)}
                    onChange={item => {
                      setFieldValue('statusData', item.value);
                    }}
                  />
     
                  {touched.statusData && errors.statusData && <Text style={styles.errorText}>{errors.statusData}</Text>}

                  {values.statusData === 'option2' && (<View>
                    <Text style={styles.text}>Assigned To</Text>
                  <Dropdown
                    style={styles.input}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    data={assignedtodata}
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!hover ? 'Select assigned to' : '...'}
                    value={values.assignedto}
                    onFocus={() => setHover(true)}
                    onBlur={() => setHover(false)}
                    onChange={item => {
                      setFieldValue('assignedto', item.value);
                    }}
                  />
                  {touched.assignedto && errors.assignedto && <Text style={styles.errorText}>{errors.assignedto}</Text>}

                  </View>
                )}
<ATRpreliminary/>
              
               <Text style={styles.text}>Final Remarks</Text>
               <TextInput
                 style={styles.input}
                 placeholder="Final Remarks"
                 onChangeText={handleChange('Finalremarks')}
                 onBlur={handleBlur('Finalremarks')}
                 value={values.Finalremarks}
                 multiline
                 numberOfLines={4}                 
               />
               {touched.Finalremarks && errors.Finalremarks && <Text style={styles.errorText}>{errors.Finalremarks}</Text>}


               <View style={styles.buttonview}>
              <TouchableOpacity  onPress={handleSubmit}>
              <Text title="Submit" style={styles.but}>SUBMIT</Text>
              </TouchableOpacity>
              </View>

                  <View>
              <Text style={styles.brand}>@Powered By <Text style={{color:'green'}}  onPress={() => openLink('https://www.ridhitek.com')}>Ridhitek</Text></Text>
              </View>
                </View>

              )}
            </Formik>
            
              </View>
             
              </View>
    </View>
    </ScrollView>
  )
}
const styles= StyleSheet.create({
    cont:{
      paddingVertical: wp('2%'),
      paddingHorizontal: wp('2%'),
      
    },
    container:{
        flex:0,
        justifyContent:'center',
      paddingLeft:'5%',
        width:'95%',
        
    },
    headerContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: hp('3%'),
    },
   
    
    header: {
      textAlign: "center",
     fontWeight:'bold',
      fontSize: 16,
      borderBottomWidth: 2,
      padding:'2%',
      marginBottom: hp('3%'),
    },
   input: {
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    marginBottom: 10,
    borderRadius: 5,
  },
  input2: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    padding: 10,
    backgroundColor: '#f0f0f0',
    color: '#333',
  },
  fileButton: {
    backgroundColor: '#007BFF',
    padding: 10,
    borderRadius: 5,
    alignItems: 'center',
    marginTop: 10,
  },
  fileButtonText: {
    color: '#fff',
  },
  errorText: {
    color: 'red',
    marginBottom: 10,
  },
  text:{
    fontWeight:'bold',
    fontSize:16,
    paddingVertical:'2%'
  },
  upload:{
    backgroundColor:'gray',
    width:'40%',
    fontSize:16,
    color:'white',
    paddingVertical:'5%',
    textAlign:'center',
    marginBottom:'3%',
    fontWeight:'700',
  },
  dropdown: {
    marginBottom: '5%',
    
    borderColor: 'lightgray',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor:'whitesmoke',
   
    
  },
  label: {
    fontSize: 18,
    marginBottom: 10,
   
  },
  dropDownContainer: {
   
    backgroundColor:'whitesmoke',
   
    position:'relative',
    
  },
  file: {
    marginTop: hp('1%'),
    borderWidth: 1,
    borderColor: '#ccc',
    marginBottom: hp('2%'),
    backgroundColor: '#fafafa',
    padding: wp('3%'),
  },
  
  brand:{
    textAlign:'center',
    paddingBottom:'8%',
    paddingTop:'5%'
  },
  centeredView: {
    flex: 1,
  
  },
  modalView: {
    margin: 0,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    textAlign:"center"
  },
  buttonOpen: {
    backgroundColor: '#dddddd',
    width:300
  },
  buttonClose: {
    backgroundColor: 'green',
   

  },
  textStyle: {
    color: 'black',
    fontWeight: 'bold',
    textAlign:"center"
  },
  modalText: {
    marginBottom: 15,
    
  },
  but:{
    fontSize:16,
    fontWeight:'bold',
    backgroundColor:'green',
    padding:'4%',
    textAlign:'center',
    color:'white',
    borderRadius:10,
  },
  buttonview:{
    paddingBottom:'3%'
  },
})

export default DcForm;
















