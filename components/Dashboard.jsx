import React, { useRef, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Animated, ScrollView, Linking , } from 'react-native';
import * as Animatable from 'react-native-animatable';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';


const Dashboard = ({navigation}) => {
  const animatedValue = useRef(new Animated.Value(0)).current;
  const textWidth = 400;
  const startAnimation = () => {
    Animated.loop(
      Animated.timing(animatedValue, {
        toValue: 1,
        duration: 15000, 
        useNativeDriver: true,
      })
    ).start();
  };
  useEffect(() => {
    startAnimation();
  }, []);
  return (
    <ScrollView contentContainerStyle={styles.scrollViewContainer} showsVerticalScrollIndicator={false}>
    <View style={styles.container}>
    
    <View style={styles.marqueview}>
    <ScrollView
    horizontal
    showsHorizontalScrollIndicator={false}
    style={{ overflow: 'hidden', flexDirection: 'row' }}>
    <Animated.View style={{
      transform: [
        {
          translateX: animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [textWidth, -400], 
          }),
        },
      ],
    }}>
      <Text style={styles.marque}>WELCOME TO ANDHRAPRADESH EXCISE COMPLAINTMANAGEMENT SYSTEM</Text>
    </Animated.View>
  </ScrollView>
    </View>
    <View style={styles.touchview} >
            <TouchableOpacity style={styles.touchbut} onPress={()=>{navigation.navigate('PublicRaiseComplaint')}}>
                  <Animatable.Text style={styles.complaint} animation="zoomIn" delay={1000}>Raise A Complaint</Animatable.Text>
            </TouchableOpacity>
        </View>
        <View style={styles.imagecontainer}>
          <Image source={require('../assets/capitalimage.jpg')} style={styles.image} />
        </View>
        <View>
            <View style={styles.latest}>
                <Text style={styles.text}>Latest Bolgs</Text>
                <ScrollView style={styles.blogScrollView}>
                <View style={styles.blogContent}>
                <View style={styles.iconsview}>
                  <FontAwesome5 name='hand-point-right' size={15} style={styles.icon}/>
                  <Text style={styles.text1}>The Prohibition and Excise Department is a major revenue earning department of the State.</Text>
                  </View>
                  <View style={styles.iconsview}>
                  <FontAwesome5 name='hand-point-right' size={15} style={styles.icon}/>
                  <Text style={styles.text1}>The Prohibition and Excise Department is a major revenue earning department of the State.</Text>
                  </View>
                  <View style={styles.iconsview}>
                  <FontAwesome5 name='hand-point-right' size={15} style={styles.icon}/>
                  <Text style={styles.text1}>The Prohibition and Excise Department is a major revenue earning department of the State.</Text>
                  </View>
                  <View style={styles.iconsview}>
                  <FontAwesome5 name='hand-point-right' size={15} style={styles.icon}/>
                  <Text style={styles.text1}>The Prohibition and Excise Department is a major revenue earning department of the State.</Text>
                  </View>
                  <View style={styles.iconsview}>
                  <FontAwesome5 name='hand-point-right' size={15} style={styles.icon}/>
                  <Text style={styles.text1}>The Prohibition and Excise Department is a major revenue earning department of the State.</Text>
                  </View>
                  
                  {/* Add more blog entries as needed */}
                </View>
              </ScrollView>
            </View>
            <View style={styles.latest}>
                <Text  style={styles.text}>Functions</Text>
                <View style={styles.blogContent}>
                      <View style={styles.iconsview}>
                      <Entypo name='dot-single' size={15} style={styles.icon}/>
                      <Text style={styles.text1}>Regulating the Production, Manufacture, Possession, Transport, Import, Export, Purchase and Sale of Liquor (wholesale trade of liquor)</Text>
                      </View>
                      <View style={styles.iconsview}>
                      <Entypo name='dot-single' size={15} style={styles.icon}/>
                      <Text style={styles.text1}>Control of Excise related crimes through detection, prevention, investigation and prosecution of offences under the law</Text>
                      </View>
                      <View style={styles.iconsview}>
                      <Entypo name='dot-single' size={15} style={styles.icon}/>
                       <Text style={styles.text1}>Prevention of illicit traffic in Narcotic Drugs and Psychotropic substances under N. D. P. S. Act, 1985.</Text>
                      </View>
                  </View>
            </View>


            <View style={styles.aboutus}>
                  <View>
                      <Text style={styles.text}>About Us</Text>
                  </View>
                  <View>
                  <Text style={styles.text2}>Prohibition & Excise Department deals with regulation of production, manufacture, possession, transport, purchase and sale of intoxicating liquor and drugs, the levy of duties of excise and countervailing duties on alcoholic liquors for human consumption and opium, Indian hemp and other narcotic drugs and narcotics and matters connected therewith in the State of Andhra Pradesh.

Prohibition & Excise Department plays the dual role of enforcing prohibition of country liquor/Illicit Distilled Liquor (Arrack) and procuring revenue through regulation of Indian Made Foreign Liquor (IMFL), Foreign Liquor (FL) and Toddy. The department, along with other State and Central Government agencies also enforces the law relating to Narcotic Drugs and Psychotropic Substances. Excise Revenue is the second largest source of revenue to the Government of Andhra Pradesh.<Text onPress={()=>navigation.navigate('AboutUs')} style={{color:'green'}}>...ViewMore</Text></Text>
                    
                  </View>
            </View>
        </View>
        <View>
              <Text style={styles.text3} onPress={()=>{Linking.openURL('https://www.ridhitek.com')}}>@ Powered By <Text style={{color:'green'}}>Ridhitek</Text></Text>
        </View>
    </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  scrollViewContainer: {
    flexGrow: 1,
  },
  container:{
    width:'100%',
   
  },
  
  imagecontainer:{
      width:'95%',
      marginHorizontal:'auto',
    
  },
  image:{
    width:'100%',
    height:400,
    opacity:0.4,
    borderRadius:10,
    
  },
  touchbut:{
      
      width:'55%',
      borderRadius:10,
      position:'absolute',
      top:350,
      left:'22%',
      zIndex:1,
  },
  complaint:{
      fontSize:20,
      fontWeight:'700',
      color:'white',
      padding:'4%',
      backgroundColor:'green',
      borderRadius:10,
      
      
  },
  marqueview: {
    width: '95%',
    marginHorizontal: 'auto',
    overflow: 'hidden',
    backgroundColor:'white'
  },
  marque: {
    fontSize: 8,
    fontWeight: '700',
    color: 'blue',
    paddingVertical: 10,
  },
  text:{
    paddingVertical:'4%',
    fontSize:20,
    fontWeight:'700',
    textAlign:'center',
    backgroundColor:'white',
    borderRadius:10
  },
  blogScrollView: {
    height: 200,
    marginTop:10,
  },
  blogContent: {
    width:'100%',
    paddingHorizontal:'5%',
  },
  text1:{
    fontSize:10,
    paddingVertical:'2%',
    fontWeight:'500',
    // textAlign:'center'
  },
  latest:{
    marginTop:'5%',
    width:'95%',
    margin:'auto',
    borderColor:'gray',
    borderWidth:1,
    borderRadius:10,
  },
  aboutus:{
    marginTop:'5%',
    width:'95%',
    margin:'auto',
    borderColor:'gray',
    borderWidth:1,
    borderRadius:10,
  },
  text2:{
    paddingVertical:'3%',
    fontSize:10,
    fontWeight:'500',
    paddingHorizontal:'5%',
    lineHeight:16,
  },

  icon:{
    paddingVertical:'3%',
    paddingRight:'5%',
    color:'green'

  },
  iconsview:{
    flex:0,
    flexDirection:'row',
    justifyContent:'space-evenly'
  },
  text3:{
    fontSize:12,
    fontWeight:'300',
    textAlign:'center',
    paddingVertical:'5%'
  }
})


  
export default Dashboard


