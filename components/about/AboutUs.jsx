import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, FlatList, Dimensions, Linking } from 'react-native';
import React, { useState, useEffect, useRef } from 'react';
import ImageList from './ImageList';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const { width, height } = Dimensions.get('window');

const AboutUs = () => {
  const openLink = (url) => {
    Linking.openURL(url).catch((err) => console.error("Failed to open URL:", err));
  };

  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer} showsVerticalScrollIndicator={false}>
        <View style={styles.imageContainer}>
          <Image source={require('../../assets/img_list/banner_excise.jpg')} style={styles.img} />
        </View>
        <View style={styles.borderContainer}>
          <Text style={styles.subhead}>About Excise Department</Text>
          <Text style={styles.description}>
            The Excise Department of Andhra Pradesh is responsible for enforcing laws related to the production, distribution, and consumption of alcoholic beverages, tobacco products, and other controlled substances. Our mission is to ensure public safety, promote responsible consumption, and prevent illegal activities.
          </Text>
        </View>
        <View style={styles.borderContainer}>
          <Text style={styles.subhead}>Mission</Text>
          <Text style={styles.description}>
            Our mission is to protect public health and safety by regulating and controlling the manufacture, sale, and distribution of alcohol and tobacco products. We aim to promote responsible consumption and prevent illegal activities through effective enforcement and education.
          </Text>
        </View>
        <View style={styles.borderContainer}>
          <Text style={styles.subhead}>Vision</Text>
          <Text style={styles.description}>
            Our vision is to create a safe and healthy environment for the citizens of Andhra Pradesh by ensuring compliance with excise laws, reducing illegal activities, and promoting responsible consumption of excisable goods.
          </Text>
        </View>
        <View style={styles.borderContainer}>
          <Text style={styles.subhead}>Functions</Text>
          <Text style={styles.description}>
            - Regulation and control of the production and distribution of alcohol and tobacco products.
          </Text>
          <Text style={styles.description}>
            - Issuance of licenses for the sale of excisable goods.
          </Text>
          <Text style={styles.description}>
            - Enforcement of laws and regulations related to excisable goods.
          </Text>
          <Text style={styles.description}>
            - Conducting inspections and investigations to ensure compliance.
          </Text>
          <Text style={styles.description}>
            - Educating the public about the risks associated with alcohol and tobacco consumption.
          </Text>
          <Text style={styles.description}>
            - Collaborating with other government agencies and law enforcement to prevent illegal activities.
          </Text>
        </View>
        <ImageList />
        <View style={styles.footer}>
          <Text style={styles.footerText}>Designed, Developed and Hosted by Ridhitek Pvt. Ltd.</Text>
          <Text style={styles.footerText}>e-Governance - Web Portal, Copyright © 2024 Andhra Pradesh Excise Department, India | All rights reserved.</Text>
          <Text style={styles.footerText}>Best viewed in Chrome v-87.0.4280.141, Microsoft Edge v-87.0.664.75, Firefox v-83.0 Browsers. Resolution: 1280x800 to 1920x1080</Text>
          <Text style={styles.footerText}>Version: 1.0.3 | Last Updated On 10-06-2024 18:35 PM</Text>
        </View>

        <View style={styles.socialIconsContainer}>
          <TouchableOpacity onPress={() => openLink('https://www.facebook.com')} style={styles.iconContainer}>
            <FontAwesome name="facebook" size={30} color="#3b5998" style={styles.icon} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => openLink('https://www.twitter.com')} style={styles.iconContainer}>
            <FontAwesome name="twitter" size={30} color="#00acee" style={styles.icon} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => openLink('https://www.youtube.com')} style={styles.iconContainer}>
            <FontAwesome name="youtube" size={30} color="#FF0000" style={styles.icon} />
          </TouchableOpacity>
          
        </View>

        <View>
          <Text style={styles.brand}>@Powered By <Text style={{ color: 'green' }} onPress={() => openLink('https://www.ridhitek.com')}>Ridhitek</Text></Text>
        </View>
      </ScrollView>
    </View>
  );
}

export default AboutUs;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#f8f8f8',
  },
  logo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    padding: 20,
  },
  iconContainer: {
    width: 50,
    height: 50,
    backgroundColor: "#dddddd",
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
  },
  socialIconsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  imageContainer: {
    alignItems: 'center',
    marginBottom: 20,
    position: 'relative',
  },
  img: {
    width: width - 40,
    height: height * 0.25,
    resizeMode: 'cover',
    borderRadius: 10,
  },
  scrollContainer: {
    paddingBottom: 20,
  },
  texthead: {
    color: 'green',
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  subhead: {
    color: 'darkgreen',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
  },
  description: {
    fontSize: 16,
    textAlign: 'justify',
    marginBottom: 10,
    lineHeight: 24,
    color: '#444',
  },
  borderContainer: {
    padding: 20,
    backgroundColor: '#e8f5e9',
    borderRadius: 10,
    marginTop: 20,
    marginBottom: 20,
  },
  footer: {
    padding: 20,
    backgroundColor: '#2c3e50',
    alignItems: 'center',
    marginTop: 40,
    borderRadius: 10,
  },
  footerText: {
    fontSize: 16,
    color: '#ecf0f1',
    textAlign: 'center',
    marginBottom: 5,
  },
  brand: {
    textAlign: 'center',
    paddingBottom: '1%',
    paddingTop: '8%',
  },
});