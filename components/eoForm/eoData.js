export const eoInitialValues = {
    complaintId:'',
    complaintDetails:'',
    complaintType:'',
    attachments:'',
    mandal:'',
    village:'',
    receivedTimeStamp:'',
    status:'',
    atrPreliminary:'',
    crimeDetectionStatus:'',
    atr:'',
    atrAttachments:'',
    casesBooked:'',
    personsArrested:'',
    seizureType:'',
    vehiclesSeized:'',
    contrabandType:'',
    quantitySeized:'',
    illicitLiquor:'',
    us:'',
}

export const eoModelMandatoryValues = [
    {
        id:1,
        placeholder:'Crime Location',
        type:'text',
        componentType:'text',
        initialValues:eoInitialValues,
        name:'crimeLocation',
    },
    {
        id:2,
        placeholder:'CR',
        type:'text',
        componentType:'text',
        initialValues:eoInitialValues,
        name:'cr',
    },
    {
        id:3,
        placeholder:'Cases Booked',
        type:'text',
        componentType:'text',
        initialValues:eoInitialValues,
        name:'casesBooked',
    },
    {
        id:4,
        placeholder:'U/S',
        type:'text',
        componentType:'text',
        initialValues:eoInitialValues,
        name:'us',
    },
    {
        id:5,
        placeholder:'Persons Arrested',
        type:'numeric',
        componentType:'text',
        initialValues:eoInitialValues,
        name:'personsArrested',
    },
    {
        id:6,
        placeholder:'Contraband Seized',
        type:'numeric',
        componentType:'text',
        initialValues:eoInitialValues,
        name:'contrabandSeized',
    },
    {
        id:7,
        placeholder:'Contraband Value',
        type:'numeric',
        componentType:'text',
        initialValues:eoInitialValues,
        name:'contrabandValue',
    }
]
export const eoModelInitialValues = {
    inspectionDate:'',
    inspectingOfficer:'',
    crimeDetectionStatus:'',
    crimeLocation:'',
    cr:'',
    casesBooked:'',
    us:'',
    personsArrested:'',
    contrabandSeized:'',
    contrabandValue:'',
    remarks:''
}
export const eoModalMustFields = [
    {
        id:1,
        name:'inspectionDate',
        placeholder:'Inspection Date',
        type:'text',            
        componentType:'text',   
        editable:true,
        label:'Inspection Date',
        initialValues:eoModelInitialValues,
        multiline:false,
    },
    {
        id:2,
        name:'inspectingOfficer',
        placeholder:'Inspecting Officer',
        type:'text',            
        componentType:'text',   
        editable:true,
        label:'Inspecting Officer',
        initialValues:eoModelInitialValues,
        multiline:false,
    },
    {
        id:3,
        name:'crimeDetectionStatus',
        placeholder:'Crime Detection Status',
        type:'text',            
        componentType:'singleSelect',   
        editable:true,
        label:'Crime Detection Status',
        initialValues:eoModelInitialValues,
        multiline:false,
    },
    {
        id:4,
        name:'remarks',
        placeholder:'Remarks',
        type:'text',            
        componentType:'text',   
        editable:true,
        label:'Remarks',
        initialValues:eoModelInitialValues,
        multiline:false,
    },
]

const complaintListObj = {
    complaintType:[
        {
            id:1,
            label:'ID Liquor',
            value:'idLiquor',
        },
        {
            id:2,
            label:'Unauthorised IML',
            value:'unAuthorizedIml',
        },
        {
            id:3,
            label:'Toddy Adulteration',
            value:'toddyAdulteration',
        },
        {
            id:4,
            label:'A4 Shop Violation',
            value:'aFourShopViolation',
        },
        {
            id:5,
            label:'2B Violation',
            value:'twoBViolation',
        },
        {
            id:6,
            label:'Defence Liquor',
            value:'defenceLiquor',
        },
        {
            id:7,
            label:'NDPL',
            value:'ndpl',
        },
        {
            id:8,
            label:'Spurious Liquor',
            value:'spuriousLiquor',
        },
        {
            id:9,
            label:'RS/ENA',
            value:'rsOrena',
        },
        {
            id:10,
            label:'DS',
            value:'ds',
        },
        {
            id:11,
            label:'Ganja',
            value:'ganja',
        },
        {
            id:12,
            label:'Excise Personnel',
            value:'excisePersonnel',
        },
        {
            id:13,
            label:'Others',
            value:'others',
        },
    ],
    status:[
        {
            id:1,
            label:'Assigned',
            value:'Assigned',
        },
        {
            id:2,
            label:'ATR Preliminary Submitted',
            value:'ATR Preliminary Submitted',
        },
        {
            id:3,
            label:'ATR Submitted',
            value:'ATR Submitted',
        },
    ],
    seizureType:[
        {
            id:1,
            label:'Vehicles',
            value:'vehicles',
        },
        {
            id:2,
            label:'Contraband',
            value:'contraband',
        },
        {
            id:3,
            label:'Others',
            value:'others',
        },
    ],
    contrabandType:[
        {
            id:1,
            label:'Illicit Liquor',
            value:'illicitLiquor',
        },
        {
            id:2,
            label:'Beer',
            value:'beer',
        },
        {
            id:3,
            label:'Jaggery',
            value:'jaggery',
        },
        {
            id:4,
            label:'Ganja',
            value:'ganja',
        },
    ],
    crimeDetectionStatus:[
        {
            id:1,
            label:'Detected',
            value:'detected',
        },
        {
            id:1,
            label:'not Detected',
            value:'notDetected',
        },
        
    ]
}
export const eoSelectList = (name)=>{
    if(Object.keys(complaintListObj).includes(name)) return complaintListObj[name];

    return [];
}


export const nonEditData = {
    complaintId:'123',
    complaintDetails:'Complaint Details are from public which is not editable by the officer!',
    mandal:'Pitapuram',
    village:'Tirupathi',
    receivedTimeStamp:'2:30pm 12-5-23',
    complaintType:'Defence Liquor'
}
export const eoData = [
    {
        id:1,
        name:'complaintId',
        placeholder:'Complaint ID',
        type:'numeric',
        componentType:'text',
        editable:false,
        label:'Complaint ID',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:2,
        name:'complaintDetails',
        placeholder:'Complaint Details',
        type:'text',
        componentType:'text', 
        editable:false,  //non editable
        label:'Complaint Details',
        initialValues:eoInitialValues,
        multiline:true,
    },
    {
        id:4,
        name:'attachments',  //attachments from public and is noneditable downloadable button
        placeholder:'Public Complaint Attachments',
        type:'text',
        componentType:'downloadFiles',
        editable:true,  //non editable
        label:'Public Complaint Attachments',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:5,
        name:'mandal',
        placeholder:'Mandal',
        type:'text',
        componentType:'text',
        editable:false,  //non edit
        label:'Mandal',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:6,
        name:'village',
        placeholder:'Village', //non edit
        type:'text',
        componentType:'text',
        editable:false,
        label:'Village',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:7,
        name:'receivedTimeStamp',
        placeholder:'Received Time Stamp',
        type:'text',            // date which is assigned so non editable
        componentType:'text',   
        editable:false,
        label:'Received Time Stamp',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:3,   //value should be default coming from backend but if user changes, should be updated
        name:'complaintType',
        placeholder:'Complaint Type',
        type:'text',
        componentType:'singleSelect', //dropdown values from control room
        editable:true,
        label:'Complaint Type',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:8,
        name:'atrPreliminary',
        placeholder:'ATR Preliminary',
        type:'text',
        componentType:'modal',   //btn on clicking popup should be uploaded
        editable:true,
        label:'ATR Preliminary',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:9,
        name:'status',
        placeholder:'Status',
        type:'text',            
        componentType:'singleSelect',   //drop down list
        editable:true,
        label:'Status',
        initialValues:eoInitialValues,
        multiline:false,
    },
    // {
    //     id:10, //should be displayed in atr preliminary popup
    //     name:'crimeDetectionStatus',
    //     placeholder:'Crime Detection Status',
    //     type:'text',
    //     componentType:'singleSelect',   //drop down => values are detected, not detected
    //     editable:true,
    //     label:'Crime Detection Status',
    //     initialValues:eoInitialValues,
    //     multiline:false,
    // },
    {
        id:11,  //if crimeDetectionStatus is detected make it mandatory field whose text is 1000char size
        name:'atr',
        placeholder:'ATR',
        type:'text',
        componentType:'text',
        editable:true,
        label:'ATR',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:12, //multiple upload files of any format upto 2mb size 
        name:'atrAttachments',
        placeholder:'ATR Attachments',
        type:'text',
        componentType:'multipleFiles',  //multiple upload file
        editable:true,
        label:'ATR Attachments',
        initialValues:eoInitialValues,
        multiline:false,
    },
    // {
    //     id:13,
    //     name:'casesBooked',
    //     placeholder:'Cases booked',
    //     type:'numeric',
    //     componentType:'text',       //numeric field
    //     editable:true,
    //     label:'Cases booked',
    //     initialValues:eoInitialValues,
    //     multiline:false,
    // },
    // {
    //     id:14,
    //     name:'personsArrested',
    //     placeholder:'Persons arrested',
    //     type:'numeric',     //numeric
    //     componentType:'text',
    //     editable:true,
    //     label:'Persons arrested',
    //     initialValues:eoInitialValues,
    //     multiline:false,
    // },
    {
        id:15,  //drop down, values are  Vehicles • Contraband • Others
        name:'seizureType', 
        placeholder:'Seizure Type',
        type:'text',
        componentType:'singleSelect',
        editable:true,
        label:'Seizure Type',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:16,  //visible to the user only when seizure type is vehicle
        name:'vehiclesSeized',
        placeholder:'Number of vehicles seized',
        type:'numeric',    //numeric
        componentType:'text',
        editable:true,
        label:'Number of vehicles seized',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:17,  // visible if seizureType is contraband 
        name:'contrabandType',
        placeholder:'Contraband Type',
        type:'text',
        componentType:'singleSelect',   //dropdown values are • Illicit Liquor • Beer • Jaggery • Ganja
        editable:true,
        label:'Contraband Type',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:18,
        name:'quantitySeized',
        placeholder:'Quantity seized (Litres)',
        type:'numeric',     //Numeric with 2 decimal points
        componentType:'text',
        editable:true,
        label:'Quantity seized (Litres)',
        initialValues:eoInitialValues,
        multiline:false,
    },
    {
        id:19,  //Field is visible only when ‘Contraband Type’ is “Illicit Liquor”
        name:'illicitLiquor',
        placeholder:'Illicit Liquor',
        type:'text',
        componentType:'text',   
        //dropdown  values are • Adulterated Toddy • NDPL • Duty Paid Liquor (IML) 
        //• Spurious Liquor • RS/ENA • ID Liquor • Wash • Others (IL)
        editable:true,
        label:'Illicit Liquor',
        initialValues:eoInitialValues,
        multiline:false,
    },
    // {
    //     id:20,
    //     name:'us',
    //     placeholder:'U/S',
    //     type:'text',    //Alpha-Numeric, Force to UPPER CASE
    //     componentType:'text',
    //     editable:true,
    //     label:'U/S',
    //     initialValues:eoInitialValues,
    //     multiline:false,
    //     uppercase:true
    // },
]

export const eoReadOnlyData = [
    {
        id:1,
        downloadBtn:false,
        name:'Complaint Id',
        value:'SKL123001122'
    },
    {
        id:2,
        downloadBtn:false,
        name:'Complaint Details',
        value:'this is about the public complaint who are illegally supplying ganja or liquour'
    },
    {
        id:3,
        downloadBtn:false,
        name:'Mandal',
        value:'Vizag'
    },
    {
        id:4,
        downloadBtn:false,
        name:'Village',
        value:'Narsapur'
    },
    {
        id:5,
        downloadBtn:false,
        name:'Complaint Time Stamp',
        value:'22/05/24, 3:30pm'
    },
    {
        id:6,
        name:'Download Complaint Files',
        value:['image.jpg','complaintDetails.doc'],
        downloadBtn:true
    },
]