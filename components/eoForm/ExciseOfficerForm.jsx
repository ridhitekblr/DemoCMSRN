import { KeyboardAvoidingView, SafeAreaView, ScrollView,  Text, View } from 'react-native'
import React, { useState } from 'react'
import { Formik } from 'formik';
import * as yup from 'yup';
import useFocus from '../../allReUsables/useFocus';
import { eoData, eoInitialValues, eoReadOnlyData, eoSelectList, nonEditData } from './eoData';
import { allReuseStyles } from '../../allReUsables/commonData/allReuseStyles';
import AllFields from '../../allReUsables/AllFields';
import ReUseButtonOne from '../../allReUsables/ReUseButtonOne';
import SubmitPopUp from '../../allReUsables/SubmitPopUp';
import ReUseEditData from '../../allReUsables/ReUseEditData';

const validationSchema = yup.object({
    // complaintId:yup.string().required('complaint Id is required!'),
    // complaintDetails:yup.string().required('complaint Details is required!'),
    // mandal:yup.string().required('mandal is required!'),
    // village:yup.string().required('village is required!'),
    // receivedTimeStamp:yup.string().required('received TimeStamp is required!'),
    // downloadComplaintFilesAttachments:yup.string().required('attachments is required!'),
    complaintType:yup.string().required('complaint Type is required!'),
    status:yup.string().required('status is required!'),
    // atrPreliminary:yup.string().required('atr Preliminary is required!'),
    // crimeDetectionStatus:yup.string().required('crime Detection Status is required!'),
    atr:yup.string().required('atr is required!'),
    atrAttachments:yup.array()
    .of(
      yup.object().shape({
        name: yup.string().required(),
        uri: yup.string().required(),
        type: yup.string().required(),
      })
    )
    .min(1, 'At least 1 file is required')
    .required('Files are required'),
    // casesBooked:yup.string().required('cases Booked is required!'),
    // personsArrested:yup.string().required('persons Arrested is required!'),
    seizureType:yup.string().required('seizure Type is required!'),
    vehiclesSeized:yup.string().required('vehicles Seized is required!'),
    contrabandType:yup.string().required('contraband Type is required!'),
    quantitySeized:yup.string().required('quantity Seized is required!'),
    illicitLiquor:yup.string().required('illicit Liquor is required!'),
    // us:yup.string().required('U/S is required!'),
})

const ExciseOfficerForm = () => {
  const [focusChange,focus] = useFocus(eoInitialValues);
  const [modalVisible,setModalVisible] = useState(false);
  const onSubmit = (values,{resetForm})=>{
    console.log('eo form',values);
    setModalVisible(true);
    resetForm();
  }
  
  return (
    <SafeAreaView>
      <KeyboardAvoidingView>
          <ScrollView>
          <Formik initialValues={eoInitialValues} 
          onSubmit={onSubmit} validationSchema={validationSchema}
          >
            {
              ({
              touched,errors,values,setFieldValue,handleSubmit,
              resetForm,handleBlur,setFieldError
              })=>(
                <View>
                  <Text style={[allReuseStyles.heading]}>Enquiry Officer Form</Text>
                  {/* <ReUseEditData data={eoReadOnlyData}/> */}
                  {
                    eoData?.map(e=>{return (
                      <AllFields key={e.name}
                      name={e.name}
                      touched={touched}
                      values={values}
                      setFieldValue={setFieldValue}
                      errors={errors}
                      handleSubmit={handleSubmit}
                      onBlur={handleBlur}
                      label={e.label}
                      value={Object.keys(nonEditData).includes(e.name)?nonEditData[e.name]: values[e.name]}
                      error={touched[e.name] && errors[e.name]}
                      placeholder={e.placeholder}
                      type={e.type}
                      multiline={e.multiline}
                      editable={e.editable}
                      componentType = {e.componentType}
                      initialValues={e.initialValues}
                      selectData={eoSelectList(e.name)}
                      search={e.search}
                      setFieldError={setFieldError}
                      uppercase={e.uppercase}
                      />
                    )}
                      
                    )
                  }
                  <ReUseButtonOne btnStyles={{color:'white',borderRadius:5,height:50}} text='Submit' onPress={handleSubmit}/>
                  <ReUseButtonOne btnStyles={{color:'white',borderRadius:5,height:50}} text='Reset' onPress={()=>{resetForm();focusChange('default','reset')}}/>
                {
                  modalVisible && 
                  <SubmitPopUp modalVisible={modalVisible} setModalVisible={setModalVisible}
        text='You have successfully submitted the complaint!'
        complaintId='SKL121180624'
        />
                }
                </View>
              )
            }
          </Formik>
          </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

export default ExciseOfficerForm;
