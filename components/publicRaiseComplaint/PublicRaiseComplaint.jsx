import { KeyboardAvoidingView, SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'
import { Formik } from 'formik';
import * as Yup from 'yup';
import { ScrollView } from 'react-native-gesture-handler';
import { registerComplaintData, registerComplaintInitialValues } from '../../allReUsables/commonData/raiseComplaintData';
import { allReuseStyles } from '../../allReUsables/commonData/allReuseStyles';
import AllFields from '../../allReUsables/AllFields';
import ReUseButtonOne from '../../allReUsables/ReUseButtonOne';
import useFocus from '../../allReUsables/useFocus';
import { allData } from "../../allReUsables/commonData/raiseComplaintData";
import SubmitPopUp from '../../allReUsables/SubmitPopUp';


const PublicRaiseComplaint = () => {
  // created custom hook to handle focus
  const [focusChange,focus] = useFocus(registerComplaintInitialValues);
  const [modalVisible, setModalVisible] = useState(false); //toggles the modal state
  
  // validation schema
  const validationSchema = Yup.object({
    name: Yup.string().required("Name is required!"),
    phone: Yup.string().required("Mobile is required!"),
    adhar: Yup.string().required("Aadhar is required!"),
    district: Yup.string().required("District is required!"),
    village: Yup.string().required("Village is required!"),
    mandal: Yup.string().required("Mandal is required!"),
    desc: Yup.string().required("Complaint description is required!"),
    attachments: Yup.array()
    .of(
      Yup.object().shape({
        name: Yup.string().required(),
        uri: Yup.string().required(),
        type: Yup.string().required(),
      })
    )
    .min(1, 'At least 1 file is required')
    .required('Files are required'),
  });

  // form validationg function i.e. onSubmit 
  const onSubmit = (values,{resetForm})=>{
    console.log('values',values);
    setModalVisible(true)
    resetForm();
  }

  return (
    // public complaint form which is the initialising page of application i.e 
    <SafeAreaView>
      <KeyboardAvoidingView>
          <ScrollView>
          <Formik initialValues={registerComplaintInitialValues} 
          onSubmit={onSubmit} validationSchema={validationSchema}
          >
            {
              ({
              touched,errors,values,setFieldValue,handleSubmit,
              resetForm,handleBlur,setFieldError
              })=>(
                <View>
                  <Text style={[allReuseStyles.heading]}>Public Complaint Form</Text>
                  {
                    registerComplaintData?.map(e=>(
                      <AllFields key={e.name}
                      name={e.name}
                      touched={touched}
                      values={values}
                      setFieldValue={setFieldValue}
                      errors={errors}
                      handleSubmit={handleSubmit}
                      onBlur={handleBlur}
                      label={e.label}
                      value={values[e.name]}
                      error={touched[e.name] && errors[e.name]}
                      placeholder={e.placeholder}
                      type={e.type}
                      multiline={e.multiline}
                      editable={e.editable}
                      componentType = {e.componentType}
                      initialValues={e.initialValues}
                      selectData={allData(e.name)}
                      search={e.search}
                      setFieldError={setFieldError}
                      />
                    ))
                  }
                  {/* form buttons to submit or reset the form */}
                  <ReUseButtonOne  text='Submit' onPress={handleSubmit}/>
                  <ReUseButtonOne  text='Reset' onPress={()=>{resetForm();focusChange('default','reset')}}/>
                {/* modal to show on successful submit */}
                {
                  modalVisible && 
                  <SubmitPopUp modalVisible={modalVisible} setModalVisible={setModalVisible}
                  text='You have successfully submitted the complaint!'
                  complaintId='SKL121180624'
                  />
                }

                </View>
              )
            }
          </Formik>
          </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

export default PublicRaiseComplaint;

