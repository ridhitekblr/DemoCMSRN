import { View, Text, TouchableOpacity,StyleSheet} from 'react-native'
import React from 'react'

export default function Agenthome({navigation}) {
  return (
    <View style={ styles.container}>
    
        <View>
            <TouchableOpacity onPress={()=> navigation.navigate('ComplaintForm')}>
            <Text style={styles.text}>Assign Complaints</Text>
            </TouchableOpacity>
        </View>
        <View>
            <TouchableOpacity onPress={()=>navigation.navigate('Agentdashboard')}>
            <Text style={styles.text}>List of Complaints</Text>
            </TouchableOpacity>
        </View>
    </View>
  )
}
const styles = StyleSheet.create({
container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
},
text:{
    fontSize:16,
    padding:'3%',
    backgroundColor:'green',
    margin:'2%',
    borderRadius:10,
    textAlign:'center',
    fontWeight:'bold',
    color:'white'
}
})