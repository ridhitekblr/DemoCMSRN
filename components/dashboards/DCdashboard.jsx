import React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text } from 'react-native';
import { dcTableData, dcTableHead} from './tableData';
import ReUsableTable from '../../allReUsables/ReUsableTable';
import { allReuseStyles } from '../../allReUsables/commonData/allReuseStyles';

export default function DCdashboard() {
  

  const rowsPerPage = 5;

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Text style={[allReuseStyles.heading]}>DC Dashboard</Text>
        <ReUsableTable tableHead={dcTableHead} 
      navigationLink='DCForm' rowsPerPage={rowsPerPage}
      tableData={dcTableData}  />
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    // paddingTop: 50,
  },
  title: {
    fontSize: 20,
    marginBottom: 20,
  },
});
