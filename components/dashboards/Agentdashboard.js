import { View, Text,StyleSheet,ScrollView} from 'react-native'
import ReUsableTable from '../../allReUsables/ReUsableTable'
import { allReuseStyles } from '../../allReUsables/commonData/allReuseStyles';
import { AGENTTableData, AGENTTableHead} from './AGENTtable';
import React from 'react'


export default function EHOdashboard() {
const rowsPerPage = 5;
return (
<View style={styles.container}>
<ScrollView>
<Text style={[allReuseStyles.heading]}>Agent Dashboard</Text>
<ReUsableTable tableHead={AGENTTableHead} tableData={AGENTTableData} rowsPerPage={rowsPerPage} navigationLink='Controller' />
</ScrollView>
</View>
)
}
const styles = StyleSheet.create({
container: {
flex: 1,
backgroundColor: '#fff',
alignItems: 'center',
justifyContent: 'center',
// paddingTop: 50,
},
title: {
fontSize: 20,
marginBottom: 20,
},
});
