import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, Alert, TouchableOpacity, ScrollView,Image ,StatusBar,Linking} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import * as DocumentPicker from 'expo-document-picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Formik } from 'formik';
import * as Yup from 'yup';

const districts = ["Srikakulam", "Vizianagaram", "Visakhapatnam", "Krishna", "Guntur","Srikakulam", "Vizianagaram", "Visakhapatnam", "Krishna", "Guntur","Srikakulam", "Vizianagaram", "Visakhapatnam", "Krishna", "Guntur",];
const mandals = { "Srikakulam": ["Mandal1", "Mandal2"], "Vizianagaram": ["Mandal3", "Mandal4"],"Visakhapatnam": ["Mandal5", "Mandal6"], "Krishna": ["Mandal7", "Mandal8"],"Guntur": ["Mandal9", "Mandal10"],  };
const villages = { "Mandal1": ["Village1", "Village2"], "Mandal2": ["Village3", "Village4"], "Mandal3": ["Village5", "Village6"], "Mandal4": ["Village7", "Village8"] };

const validationSchema = Yup.object().shape({
  name: Yup.string()
  .required('Name is required'),
  phone: Yup.string()
  .min(10,'Must be exactly 10 digits')
  .max(10,'Must be exactly 10 digits')
  .matches(/^[0-9]{10}$/, 'Please enter a valid phone number').required('Phone number is required'),
  aadhaar: Yup.string()
  .min(12,'Must be exactly 12 digits')
  .max(12,'Must be exactly 12 digits')
  .matches(/^[0-9]{12}$/, 'Please enter a valid Aadhaar number').optional(),
  district: Yup.string().required('District is required'),
  mandal: Yup.string()
  .min(4,'Must be exactly 4 digits')
  .required('Mandal is required'),
  village: Yup.string().required('Village is required'),
  complaintDetails: Yup.string().required('Complaint details are required').max(1000, 'Complaint details can\'t exceed 1000 characters'),
  complaintSource: Yup.string().required('Complaint source is required'),
  selectedFile: Yup.mixed().required(''),
});

const ComplaintForm = () => {
  const [selectedFile, setSelectedFile] = useState(null);

  const handleFilePick = async (setFieldValue) => {
    try {
      let result = await DocumentPicker.getDocumentAsync({
        type: ['image/*', 'application/pdf'],
        copyToCacheDirectory: true,
      });
      console.log('result',result);
      if(!result.success){
        const assets = result.assets;
        const name = assets[0].name
        setFieldValue('selectedFile', name)
      }
   
    } catch (error) {
      Alert.alert('Error', 'An error occurred while picking the file');
      console.error('File pick error:', error);
    }
  };
  
  const openLink = (url) => {
    Linking.openURL(url).catch((err) => console.error("Failed to open URL:", err));
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>

        <Text style={styles.header}>COMPLAINT REGISTRATION FORM FOR ‘Agent’</Text>
        <Formik
          initialValues={{
            name: '',
            phone: '',
            aadhaar: '',
            district: '',
            mandal: '',
            village: '',
            complaintDetails: '',
            complaintSource: '',
            selectedFile: '',
          }}
          validationSchema={validationSchema}
          onSubmit={(values) => {
            Alert.alert("Success", "Form submitted successfully!",);
            
          }}
        >
          {({ handleChange, handleBlur, handleSubmit, setFieldTouched,setFieldValue, values, errors, touched }) => (
            <View>



              <Text style={styles.text}>Name:</Text>
              <TextInput
                style={[styles.input, { textTransform: 'uppercase' }]}
                onChangeText={handleChange('name')}
                
             onBlur={()=> setFieldTouched('name')}
                value={values.name}
              />
              {errors.name && touched.name && <Text style={styles.errorText}>{errors.name}</Text>}

              <Text style={styles.text}>Phone :</Text>
              <TextInput
                style={styles.input}
                onChangeText={handleChange('phone')}
              
                onBlur={()=> setFieldTouched('phone')}
                value={values.phone}
                keyboardType="numeric"
              />
              {errors.phone && touched.phone && <Text style={styles.errorText}>{errors.phone}</Text>}

              <Text style={styles.text}>Aadhaar:</Text>
              <TextInput
                style={styles.input}
                onChangeText={handleChange('aadhaar')}
              
                onBlur={()=> setFieldTouched('aadhaar')}
                value={values.aadhaar}
                keyboardType="numeric"
              />
              {errors.aadhaar && touched.aadhaar && <Text style={styles.errorText}>{errors.aadhaar}</Text>}

              <Text style={styles.text}>District:</Text>
              <Picker
                selectedValue={values.district}
                style={styles.picker}
                 listMode="SCROLLVIEW"
                 onBlur={()=> setFieldTouched('district')}
                onValueChange={(itemValue) => {
                  setFieldValue('district', itemValue);
                  setFieldValue('mandal', '');
                  setFieldValue('village', '');
                    
                }}
              >
                <Picker.Item label="Select District" value="" />
                {districts.map((district) => (
                  <Picker.Item label={district} value={district} key={district} />
                ))}
              </Picker>
              {errors.district && touched.district && <Text style={styles.errorText}>{errors.district}</Text>}

              <Text style={styles.text}>Mandal:</Text>
              <Picker
                selectedValue={values.mandal}
                style={[styles.picker,{borderWidth:2 ,borderColor:"black"}]}
                onValueChange={(itemValue) => {
                  setFieldValue('mandal', itemValue);
                  setFieldValue('village', '');
                }}
                enabled={!!values.district}
              >
                <Picker.Item label="Select Mandal" value="" />
                {(mandals[values.district] || []).map((mandal) => (
                  <Picker.Item label={mandal} value={mandal} key={mandal} />
                ))}
              </Picker>
              {errors.mandal && touched.mandal && <Text style={styles.errorText}>{errors.mandal}</Text>}

              <Text style={styles.text}>Village:</Text>
              <Picker
                selectedValue={values.village}
                style={styles.picker}
                onValueChange={handleChange('village')}
                enabled={!!values.mandal}
              >
                <Picker.Item label="Select Village" value="" />
                {(villages[values.mandal] || []).map((village) => (
                  <Picker.Item label={village} value={village} key={village} />
                ))}
              </Picker>
              {errors.village && touched.village && <Text style={styles.errorText}>{errors.village}</Text>}

              <Text style={styles.text}>Complaint Details:</Text>
              <TextInput
                style={[styles.input, styles.textArea]}
                onChangeText={handleChange('complaintDetails')}
                
                onBlur={()=> setFieldTouched('complaintDetails')}
                value={values.complaintDetails}
                multiline
                numberOfLines={5}
              />
              {errors.complaintDetails && touched.complaintDetails && <Text style={styles.errorText}>{errors.complaintDetails}</Text>}

             

           <Text>Attachment:</Text>
      <TouchableOpacity onPress={() => handleFilePick(setFieldValue)} style={styles.file}>
        <Text >Attachment File</Text>
      </TouchableOpacity>
      <Text>{values.selectedFile}</Text>
      
      {errors.selectedFile && touched.selectedFile && <Text style={styles.errorText}>{errors.selectedFile}</Text>}

              <Text style={styles.text}>Complaint Source:</Text>
              <Picker
                selectedValue={values.complaintSource}
                style={styles.picker}
                onValueChange={handleChange('complaintSource')}
              >
                <Picker.Item label="Select Source" value="" />
                <Picker.Item label="Phone" value="Phone" />
                <Picker.Item label="In-writing" value="In-writing" />
                <Picker.Item label="Email" value="Email" />
                <Picker.Item label="SMS" value="SMS" />
                <Picker.Item label="Press/e-Media" value="Press/e-Media" />
                <Picker.Item label="EC" value="EC" />
              </Picker>
              {errors.complaintSource && touched.complaintSource && <Text style={styles.errorText}>{errors.complaintSource}</Text>}
             <View style={styles.buttonview}>
              <TouchableOpacity  onPress={handleSubmit}>
              <Text title="Submit" style={styles.but}>SUBMIT</Text>
              </TouchableOpacity>
              </View>
              <View>
              <Text style={styles.brand}>@Powered By <Text style={{color:'green'}} onPress={() => openLink('https://www.ridhitek.com')}>Ridhitek</Text></Text>
              </View>
            </View>
          )}
        </Formik>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: wp('3%'),
    paddingHorizontal: wp('7%'),
    
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: hp('3%'),
  },
  logo: {
    width: 90,
    height: 90,
    marginRight: 10,
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    padding: wp('3%'),
    marginBottom: hp('2%'),
    backgroundColor: '#fafafa',
    borderRadius: 5,
  },
  picker: {
    borderWidth: 1,
    borderColor: '#ccc',
    marginBottom: hp('2%'),
    backgroundColor: '#fafafa',
  },
  text:{
    fontSize:16,
    fontWeight:'bold',
    paddingVertical:'2%'
  },
  textArea: {
    height: hp('15%'),
  },
  errorText: {
    color: 'red',
    marginBottom: hp('1%'),
  },
  file: {
    marginTop: hp('1%'),
    borderWidth: 1,
    borderColor: '#ccc',
    marginBottom: hp('2%'),
    backgroundColor: '#fafafa',
    padding: wp('3%'),
  },
  header: {
    textAlign: "center",
    fontSize: 15,
    fontWeight:'bold',
    paddingBottom:'2%',
    borderBottomWidth: 2,
  },
  selectedFile: {
    marginTop: hp('1%'),
    marginBottom: hp('2%'),
  },
  but:{
    fontSize:16,
    fontWeight:'bold',
    backgroundColor:'green',
    padding:'4%',
    textAlign:'center',
    color:'white',
    borderRadius:10,
  },
  buttonview:{
    paddingBottom:'3%'
  },
  brand:{
    textAlign:'center',
    paddingBottom:'3%'
  }
});

export default ComplaintForm;







