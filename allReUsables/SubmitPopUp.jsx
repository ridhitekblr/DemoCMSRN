import React from 'react';
import { Modal, StyleSheet, Text, Pressable, View, TouchableOpacity} from 'react-native';
import {FontAwesome5} from '@expo/vector-icons';

// after form submission submit pop up is made to display the success message.
const SubmitPopUp = (props) => {
    const {modalVisible, setModalVisible,complaintId} = props;
    
    // returning the modal
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={[styles.head]}>
                <FontAwesome5 name='check-circle' style={[styles.checkIcon]}/>
                <Text style={[styles.successText]}>Success!</Text>
            </View>
            <View>
            <Text style={styles.text}>Your complaint Id is</Text>
            <Text style={styles.complaintId}>{complaintId}</Text>
            </View>
            <View style={[styles.btnContainer]}>
            <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}>
              <Text style={styles.textStyle}>Close</Text>
            </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <Pressable
        style={[styles.button, styles.buttonOpen]}
        onPress={() => setModalVisible(true)}>
        <Text style={styles.textStyle}>Show Modal</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    backgroundColor: 'lightblue',
    borderRadius: 10,
    padding: 40,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  btnContainer:{
    marginTop:30
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: 'white',
    borderRadius:5,
    width:200,
  },
  textStyle: {
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  
  head:{
    flexDirection:'row',
    justifyContent:'space-evenly',
    alignItems:'center',
    width:180,
    marginVertical:5,
  },
  checkIcon:{
    color:'green',
    fontSize:30
  },
  text:{
    fontSize:15,
    marginVertical:5,
    textAlign:'center'
  },
  complaintId:{
    textAlign:'center',
    fontSize:22
  },
  successText:{
    fontSize:30
  }
});

export default SubmitPopUp;