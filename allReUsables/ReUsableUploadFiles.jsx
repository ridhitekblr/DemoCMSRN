import React from 'react';
import { Text,  View } from 'react-native';
import * as DocumentPicker from 'expo-document-picker';
import { allReuseStyles } from './commonData/allReuseStyles';
import ReUseButtonOne from './ReUseButtonOne';

// upload files are uploaded through this component
const ReUsableUploadFiles = (props) => {
    const { label, name, setFieldValue,multipleFiles, error, value } = props;

    const handlePress = async () => {
        try {
            const results = await DocumentPicker.getDocumentAsync({
                type: '*/*', // or specify the MIME type(s) you want to support
                multiple: multipleFiles,
            });
            
            if (results.canceled === false) {
                const assets = results.assets;
                const files = assets.map((file,i)=>{return {
                    // name: file.name.split(".")[0],
                    name: file.name,
                    uri: file.uri,
                    type: file.mimeType,
                    size: file.size,
                }}) 
    // after putting all the required data in array setting the value through setFieldvalue
                setFieldValue(name, files);
            }
        } catch (err) {
            console.error('Unknown error: ', err);
        }
    };
    // returning file upload component
    return (
        <View style={[allReuseStyles.inputContainer]}>
            <Text style={[allReuseStyles.label]}>{label}</Text>
            <ReUseButtonOne text='Upload Files' onPress={handlePress} btnStyles={allReuseStyles.btnStyles}/>
            {error && <Text style={[allReuseStyles.error]}>{error}</Text>}
            {value && value.map((file, index) => (
                <Text key={index} style={allReuseStyles.label}>{file.name}</Text>
            ))}
        </View>
    );
};

export default ReUsableUploadFiles;
