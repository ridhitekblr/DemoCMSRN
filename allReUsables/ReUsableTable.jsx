import React, { useState, useEffect, useMemo } from 'react';
import { View, StyleSheet, ScrollView, Button, Text} from 'react-native';
import { Table, Row } from 'react-native-table-component';
import { useNavigation } from '@react-navigation/native';
import ReUseButtonOne from '../allReUsables/ReUseButtonOne';

// reusable table for dashboards
const ReUsableTable = (props) => {
  const { tableHead, tableData, rowsPerPage,navigationLink } = props;
  const navigation = useNavigation();
  const [currentPage, setCurrentPage] = useState(0);
  const [headWidths, setHeadWidths] = useState([]);
  const totalPages = Math.ceil(tableData.length / rowsPerPage);

  const handlePrevious = () => {
    setCurrentPage((prev) => Math.max(prev - 1, 0));
  };

  const handleNext = () => {
    setCurrentPage((prev) => Math.min(prev + 1, totalPages - 1));
  };

  const getDataForPage = useMemo(() => {
    const start = currentPage * rowsPerPage;
    const end = start + rowsPerPage;
    return tableData.slice(start, end);
  }, [currentPage, rowsPerPage, tableData]);

  useEffect(() => {
    const widths = tableHead.map((head, index) => {
      const headerWidth = head.length * 10 + 20;
      const dataWidth = Math.max(
        ...tableData.map(row => {
          const cell = row[index];
          if (React.isValidElement(cell)) {
            return 24 + 20; // Icon size + padding
          }
          return cell?.toString().length * 10 + 20;
        })
      );
      return Math.max(headerWidth, dataWidth);
    });
    setHeadWidths(widths);
  }, [tableData, tableHead]);
  const btnStyles = {
    height:30,
    color:'whitesmoke',
    borderRadius:5
  }
//  render cell function when specific cell comes what to display 
  const renderCell = (cellData, index) => {
    if (tableHead[index] === 'ACTION') {
      return <ReUseButtonOne text='Assign' btnStyles={btnStyles}
      onPress={()=>navigation.navigate(navigationLink)}/>
    }else {
      return <Text style={styles.cellText}>{cellData}</Text>;
    }
  };

  // rendering the each row of the data through function
  const renderRow = (rowData, rowIndex) => (
    <View style={styles.row} key={rowIndex}>
      {rowData.map((cellData, index) => (
        <View
          key={index}
          style={{
            width: headWidths[index],
            height: 50,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
            borderColor: '#C1C0B9',
          }}
        >
          {renderCell(cellData, index)}
        </View>
      ))}
    </View>
  );

  return (
    <View style={styles.container}>
      <ScrollView horizontal>
        <View>
          <Table>
            {/* head of the table */}
            <Row
              data={tableHead}
              widthArr={headWidths}
              style={styles.head}
              textStyle={styles.headText}
            />
            {/* row data of the table where each row is mapped as per the data given from prop */}
            {getDataForPage.map((rowData, index) => renderRow(rowData, index))}
          </Table>
        </View>
      </ScrollView>
      {/* pagination for displaying previous and next buttons as well as page number */}
      <View style={styles.pagination}>
        <Button title="Previous" onPress={handlePrevious} disabled={currentPage === 0} />
        <Text style={styles.pageNumber}>
          Page {currentPage + 1} of {totalPages}
        </Text>
        <Button title="Next" onPress={handleNext} disabled={currentPage === totalPages - 1} />
      </View>
    </View>
  );
};

export default ReUsableTable;

const styles = StyleSheet.create({
  container: { flex: 1, paddingHorizontal: 20 },
  head: { height: 50, backgroundColor: '#f1f8ff' },
  headText: { margin: 6, textAlign: 'center' },
  row: { flexDirection: 'row', height: 50, borderBottomWidth: 1, borderColor: '#C1C0B9' },
  cellText: { textAlign: 'center' },
  pagination: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginVertical: 5,
    borderColor: '#ccc',
  },
  pageNumber: {},
});
