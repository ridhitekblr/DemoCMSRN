import React, { memo } from 'react';
import { Text, View } from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';
import useFocus from './useFocus';
import { allReuseStyles } from './commonData/allReuseStyles';

// wrapping the component in memo to render the component only when its props are changed.
const ReUsableSingleSelect = memo((props) => {
  const {
    search, selectData, label, value, setFieldError,
    name, placeholder, error, setFieldValue, initialValues
  } = props;

  const [focusChange, focus] = useFocus(initialValues);
// returning reusable single select component based upon props 
  return (
    <View style={allReuseStyles.container}>
      <Text style={[allReuseStyles.label]}>{label}</Text>
      <Dropdown
        style={[
          allReuseStyles.input,
          !error ? { borderColor: 'gray', borderWidth: 0.5 } : { borderColor: 'red', borderWidth: 2 }
        ]}
        placeholderStyle={allReuseStyles.selectPlaceholderStyle}
        selectedTextStyle={allReuseStyles.selectedTextStyle}
        inputSearchStyle={allReuseStyles.selectInputSearchStyle}
        iconStyle={allReuseStyles.selectIconStyle}
        data={selectData}
        search={search}
        maxHeight={300}
        labelField="label"
        valueField="value"
        placeholder={!focus[name] ? placeholder : '...'}
        searchPlaceholder="Search..."
        value={value}
        onFocus={() => focusChange(name, 'focus')}
        onBlur={() => {
          focusChange(name, 'blur');
          if (!value) setFieldError(name, `${name} is required!`);
        }}
        onChange={item => {
          setFieldValue(name, item.value);
        }}
      />
      {error && <Text style={allReuseStyles.error}>{error}</Text>}
    </View>
  );
});

export default ReUsableSingleSelect;

