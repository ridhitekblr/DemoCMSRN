import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { allReuseStyles } from './commonData/allReuseStyles';
import ReUseButtonOne from './ReUseButtonOne';

// to display the non editable data in the form of a section 
const ReUseEditData = (props) => {
    const {data} = props;
    const btnStyles = {
        width:'60%',
        bgColor:'gray',
        height:40
    }

    // returning the section of form to display the data through prop.
  return (
    <View style={[allReuseStyles.boxShadow,styles.container]}>
      <Text style={[styles.heading]}>Public Complaint Data (Read only) </Text>
      {
        data?.map((e,i)=>{
            return e?.downloadBtn ? ( <ReUseButtonOne key={i}
                text='Download Attachments' btnStyles={btnStyles}/>
            ) :
             <View style={[styles.textContainer]} key={i}>
                <Text style={[styles.text]}>{e?.name}</Text>
                <Text style={[styles.dot]}>:</Text>
                <Text style={[styles.text,styles.value]}>{e?.downloadBtn ?(
                    <ReUseButtonOne 
                    text='Download Attachments' btnStyles={btnStyles}/>
                
                ) : e?.value}</Text>
            </View>
        })
      }
    </View>
  )
}

export default ReUseEditData;

const styles = StyleSheet.create({
    container:{
        width:'90%',
        marginHorizontal:'auto',
        backgroundColor:'white',
        padding:10,
        marginVertical:10
    },
    heading:{
        fontSize:15,
        fontWeight:'bold',
        textAlign:'center'
    },
    textContainer:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    text:{
        width:'48%',
        marginVertical:5,
    },
    dot:{
        width:'4%',
        textAlign:'center',
        marginVertical:5,
    },
    value:{
        fontWeight:'bold'
    }
})