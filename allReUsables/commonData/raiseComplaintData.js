// register complaint initialValues
export const registerComplaintInitialValues = {
    name:'',
    phone:'',
    adhar:'',
    district:'',
    mandal:'',
    village:'',
    desc:'',
    attachments:''
}

// data to render each component
export const registerComplaintData = [
    {
        name:'name',
        placeholder:'Name',
        multiline:false,
        noOfLines:1,
        editable:true,
        componentType:'text',
        initialValues:registerComplaintInitialValues,
        label:'Name',
        type:'text',
    },
    {
        name:'phone',
        placeholder:'Mobile Number',
        multiline:false,
        noOfLines:1,
        editable:true,
        componentType:'text',
        initialValues:registerComplaintInitialValues,
        label:'Mobile Number',
        type:'numeric',
    },
    {
        name:'adhar',
        placeholder:'Adhar',
        multiline:false,
        noOfLines:1,
        editable:true,
        componentType:'text',
        initialValues:registerComplaintInitialValues,
        label:'Adhar number',
        type:'numeric',
    },
    {
        name:'district',
        placeholder:'District',
        multiline:false,
        noOfLines:1,
        editable:true,
        componentType:'singleSelect',
        search:true,
        initialValues:registerComplaintInitialValues,
        label:'District',
        type:'select',
    },
    {
        name:'mandal',
        placeholder:'Mandal',
        multiline:false,
        noOfLines:1,
        editable:true,
        componentType:'singleSelect',
        search:true,
        initialValues:registerComplaintInitialValues,
        label:'Mandal',
        type:'select',
    },
    {
        name:'village',
        placeholder:'Village',
        multiline:false,
        noOfLines:1,
        editable:true,
        componentType:'singleSelect',
        search:true,
        initialValues:registerComplaintInitialValues,
        label:'Village',
        type:'select',
    },
    {
        name:'desc',
        placeholder:'Description',
        multiline:true,
        noOfLines:10,
        editable:true,
        componentType:'text',
        initialValues:registerComplaintInitialValues,
        label:'Description',
        type:'text',
    },
    {
        name:'attachments',
        placeholder:'Attachments',
        multiline:false,
        noOfLines:1,
        editable:true,
        componentType:'multipleFiles',
        initialValues:registerComplaintInitialValues,
        label:'Attachments',
        type:'file',
    },
    ]
export const complaintSelectData = [
    {
        name:'district',
        placeholder:'District',
        multiline:false,
        noOfLines:1,
        type:'select',
    },
    {
        name:'mandal',
        placeholder:'Mandal',
        multiline:false,
        noOfLines:1,
        type:'select',
    },
    {
        name:'village',
        placeholder:'Village',
        multiline:false,
        noOfLines:1,
        type:'select',
    },
]

const selectObjData = {
    district:[
        {
            id:1,
            label:'Srikakulam',
            value:'Srikakulam',
        }
    ],
    mandal:[
        {
            id:1,
            label:'Vizag',
            value:'Vizag',
        }
    ],
    village:[
        {
            id:1,
            label:'Pitapuram',
            value:'Pitapuram',
        }
    ],
}

export const allData = (name)=>{

    switch(name){
        case 'district':return selectObjData.district;
        case 'mandal':return selectObjData.mandal;
        case 'village':return selectObjData.village;
    }
}