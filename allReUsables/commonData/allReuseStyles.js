
export const allReuseStyles = {
  makeCenter:{
    display:'flex',
    alignItems:'center',
    justifyContent:'center'
  },
  fs:{
    fontSize:16,
    fontWeight:'600'
  },
    heading:{
        marginHorizontal:'auto',
        fontSize:22,
        fontWeight:'700',
        marginVertical:5
    },
    inputContainer:{
        marginVertical: 10,
        width:'100%',
      },
    label:{
        fontSize:16,
        marginHorizontal:'7.5%',
        fontWeight:'500'
      },
      input: {
        height: 50,
        width:'85%',
        marginHorizontal:'auto',
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
        backgroundColor:'white',
        fontSize:14,
        placeholderFontSize:14
      },
      error:{
        marginHorizontal:'7.5%',
        color:'red',
        fontSize:13
      },
      btnStyles:{
        color:'white',
        borderRadius:5,
        margin:0
      },
      submitBtn:{
        margin:10
      },
    //   select begin
    selectDropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
      //   width:'85%'
      },
      selectIcon: {
        marginRight: 5,
      },
      selectLabel: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
      },
      selectPlaceholderStyle: {
        fontSize: 14,
      },
      selectedTextStyle: {
        fontSize: 16,
      },
      selectIconStyle: {
        width: 20,
        height: 20,
      },
      selectInputSearchStyle: {
        height: 40,
        fontSize: 16,
      },
    // select end

    // box shadow
    boxShadow:{
      shadowColor:'blue',
      shadowOffset:{width:1,height:2},
      shadowOpacity: 5,
      shadowRadius: 2,
      elevation: 10, 
},
}