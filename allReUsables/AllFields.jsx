import ReUsableInput from './ReUsableInput';
import ReUsableSingleSelect from './ReUsableSingleSelect';
import ReUsableUploadFiles from './ReUsableUploadFiles';
import DownloadFiles from './DownloadFiles';
import ModalField from './ModalField';

const AllFields = (props) => {
    
    const {componentType,...rest} = props;

    // depending upon componentType prop each reusable field is given from AllFields
    switch(componentType){
        case 'text':return <ReUsableInput {...rest}/>;
        case 'singleSelect':return <ReUsableSingleSelect {...rest}/>;
        case 'multipleFiles':return <ReUsableUploadFiles {...rest} multipleFiles={true}/>
        case 'singleFile':return <ReUsableUploadFiles {...rest} multipleFiles={false}/>
        case 'downloadFiles':return <DownloadFiles {...rest}/>
        case 'modal' :return <ModalField {...rest} />
        default :return <ReUsableInput {...rest}/>
    }
  
}

export default AllFields;
