import { Text, View } from 'react-native'
import React from 'react'
import ReUseButtonOne from './ReUseButtonOne';
import { allReuseStyles } from './commonData/allReuseStyles';

// downloadable button for upload files.
const DownloadFiles = (props) => {
    const {label,value} = props;

  return (
    <View style={[allReuseStyles.inputContainer]}>
        <Text style={[allReuseStyles.label]}>{label}</Text>
      <ReUseButtonOne text='Download complaint files' btnStyles={allReuseStyles.btnStyles}/>
      {value && <Text>{value}</Text>}
    </View>
  )
}

export default DownloadFiles;
