import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { allReuseStyles } from './commonData/allReuseStyles';

// re usable button 
const ReUseButtonOne = (props) => {

    const {text,onPress,btnStyles} = props;

    // displaying default styles as well as dynamic styles through the prop btnStyles.
    const styles = StyleSheet.create({
        container:{
            height: btnStyles?.height ?? 50,
            width: btnStyles?.width ?? '85%',
            backgroundColor: btnStyles?.bgColor ?? 'green',
            borderRadius: btnStyles?.borderRadius ?? 5,
            marginHorizontal:'auto',
            marginVertical: btnStyles?.margin ?? 10
        },
        text:{
            color:btnStyles?.color ?? 'white',
            fontSize:btnStyles?.fontSize ?? 16
            
        }
    })
    // returning the button.
  return (
    <TouchableOpacity onPress={onPress} style={[styles.container,allReuseStyles.makeCenter]}>
        <Text style={[styles.text,allReuseStyles.fs]}>{text}</Text>
    </TouchableOpacity>
  )
}

export default ReUseButtonOne;
