import { useState, useCallback } from 'react';

const useFocus = (initialValues) => {
  const [focus, setFocus] = useState(initialValues);
  
// handling focus is made reusable by making custom hook
  const focusChange = useCallback((name, action) => {
    setFocus((prevFocus) => ({
      ...prevFocus,
      [name]: action === 'focus'
    }));
  }, []);

  return [focusChange, focus];
};

export default useFocus;
