import { StyleSheet, Text, TextInput, View } from "react-native";
import React, { useState } from "react";
import useFocus from "./useFocus";
import { allReuseStyles } from "./commonData/allReuseStyles";


const ReUsableInput = (props) => {
  // setting the dynamic height to the field
  const [inputHeight, setInputHeight] = useState(100);
  const [focusChange,focus] = useFocus(props.initialValues)
  const {
    handleChange,
    name,
    type,
    placeholder,
    multiline,
    setFieldValue,
    options,
    onBlur,
    editable,
    touched,
    value,
    uppercase,
    label,
    error
  } = props;
  // dynamic height function
  const handleContentChangeHeight = (e) => {
    const newHeight = e.nativeEvent.contentSize.height;
    if (newHeight > 100) {
      setInputHeight(newHeight);
    }
  };
  // returning reusable text field by receiving all the required props to render input.
  return (
    <View style={[allReuseStyles.inputContainer]}>
      <Text style={[allReuseStyles.label]}>{label}</Text>
     <TextInput
          style={[
            allReuseStyles.input,
            multiline
              ? { height: inputHeight, textAlignVertical: "top" }
              : { height: 50 },
              !editable && {color:'black'},
              (focus[name] || error) && {borderColor:'red',borderWidth:1}
          ]}
          onContentSizeChange={handleContentChangeHeight}
          placeholder={placeholder}
          name={name}
          value={uppercase?value.toUpperCase():value}
          editable={editable}
        //   onFocus={()=>focusChange(name,'focus')}
        //   onBlur={()=>focusChange(name,'blur')}
          // onBlur={onBlur(name)}
          multiline={multiline}
          keyboardType={type}
          onChangeText={(text)=>setFieldValue(name,text)}
        />
      {error && <Text style={[allReuseStyles.error]}>{error}</Text>}
    </View>
  );
};

export default ReUsableInput;


