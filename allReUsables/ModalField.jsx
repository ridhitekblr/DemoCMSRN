import React, { useState, useCallback } from "react";
import {
  Button,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";
import {
  eoModalMustFields,
  eoModelInitialValues,
  eoSelectList,
} from "../components/eoForm/eoData";
import ReUseButtonOne from "./ReUseButtonOne";
import { allReuseStyles } from "./commonData/allReuseStyles";
import { Formik } from "formik";
import * as Yup from "yup";
import { Dropdown } from "react-native-element-dropdown";
import DateTime from "./DateTime";

// modal component for enquiry officer to display some fields based on requirement.
const ModalField = (props) => {
  const {
    label,
    values,
    setFieldError,
    onBlur,
    setFieldValue,
    placeholder,
    touched,
    errors,
  } = props;
  const [modalVisible, setModalVisible] = useState(false);
  const [isFocus, setIsFocus] = useState(eoModelInitialValues);
  const validationSchema = Yup.object({
    inspectionDate: Yup.string().required("Inspection Date is required"),
    inspectingOfficer: Yup.string().required("Inspectiong Officer is required"),
    crimeDetectionStatus: Yup.string().required(
      "Crime Detection Status is required"
    ),
    remarks: Yup.string().required("Remarks is required!"),
    crimeLocation: Yup.string().when("crimeDetection", {
      is: "detected",
      then: Yup.string().required("Crime location is required!"),
    }),
  });
  const onSubmit = (values, { resetForm }) => {
    console.log("modal values", values);
    resetForm();
  };

  const handleOpenModal = useCallback(() => setModalVisible(true), []);
  const handleCloseModal = useCallback(() => setModalVisible(false), []);

  return (
    <View style={[allReuseStyles.inputContainer]}>
      <Text style={[allReuseStyles.label]}>{label}</Text>
      <ReUseButtonOne
        text={placeholder}
        onPress={handleOpenModal}
        btnStyles={allReuseStyles.btnStyles}
      />
      {modalVisible && (
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <View style={[styles.container]}>
            <ScrollView>
              <Formik
                initialValues={eoModelInitialValues}
                onSubmit={onSubmit}
                validationSchema={validationSchema}
              >
                {({
                  setFieldError,
                  setFieldValue,
                  values,
                  errors,
                  handleSubmit,
                  resetForm,
                  touched,
                  handleBlur,
                  handleChange,
                }) => (
                  <View>
                    <Text style={[allReuseStyles.heading, { color: "black" }]}>
                      ATR PRELIMINARY FORM
                    </Text>
                    <DateTime
                      label="Inspection Date"
                      placeholder="Inspection Date"
                      name="inspectionDate"
                      value={values.inspectionDate}
                      setFieldValue={setFieldValue}
                      error={
                        touched.inspectionDate &&
                        errors.inspectionDate &&
                        errors.inspectionDate
                      }
                    />
                    <View style={[allReuseStyles.inputContainer]}>
                      <Text style={[allReuseStyles.label]}>
                        Inspecting Officer
                      </Text>
                      <TextInput
                        style={[allReuseStyles.input]}
                        onChangeText={handleChange("inspectingOfficer")}
                        placeholder="Inspecting Officer"
                        value={values.inspectingOfficer}
                        name="inspectingOfficer"
                      />
                      {touched.inspectingOfficer &&
                        errors.inspectingOfficer && (
                          <Text style={[allReuseStyles.error]}>
                            {errors.inspectingOfficer}
                          </Text>
                        )}
                    </View>

                    <View style={styles.selectContainer}>
                      <Text style={[{ fontSize: 16, fontWeight: 500 }]}>
                        Crime Detection Status
                      </Text>
                      <Dropdown
                        style={[
                          styles.dropdown,
                          isFocus.crimeDetectionStatus && {
                            borderColor: "blue",
                          },
                        ]}
                        placeholderStyle={allReuseStyles.placeholderStyle}
                        selectedTextStyle={allReuseStyles.selectedTextStyle}
                        inputSearchStyle={allReuseStyles.inputSearchStyle}
                        iconStyle={allReuseStyles.iconStyle}
                        data={[
                          { label: "Detected", value: "detected" },
                          { label: "Not Detected", value: "notDetected" },
                        ]}
                        search
                        maxHeight={300}
                        labelField="label"
                        valueField="value"
                        placeholder={
                          !isFocus.crimeDetectionStatus
                            ? "Crime Detection Status"
                            : "..."
                        }
                        searchPlaceholder="Search..."
                        value={values.crimeDetectionStatus}
                        onFocus={() => setIsFocus(true)}
                        onBlur={() => setIsFocus(false)}
                        onChange={(item) => {
                          setIsFocus(false);
                          setFieldValue("crimeDetectionStatus", item.value);
                        }}
                      />
                      {touched.crimeDetectionStatus &&
                        errors.crimeDetectionStatus && (
                          <Text style={[{ color: "red", fontSize: 12 }]}>
                            {errors.crimeDetectionStatus}
                          </Text>
                        )}
                    </View>
                    {values.crimeDetectionStatus === "detected" && (
                      <>
                        <View style={[allReuseStyles.inputContainer]}>
                          <Text style={[allReuseStyles.label]}>
                            Crime Location
                          </Text>
                          <TextInput
                            style={[allReuseStyles.input]}
                            onChangeText={handleChange("crimeLocation")}
                            value={values.crimeLocation}
                            placeholder="Crime Location"
                            name="crimeLocation"
                          />
                          {touched.crimeLocation && errors.crimeLocation && (
                            <Text style={[allReuseStyles.error]}>
                              {errors.crimeLocation}
                            </Text>
                          )}
                        </View>
                        <View style={[allReuseStyles.inputContainer]}>
                          <Text style={[allReuseStyles.label]}>CR.#</Text>
                          <TextInput
                            style={[allReuseStyles.input]}
                            onChangeText={handleChange("cr")}
                            placeholder="CR.#"
                            value={values.cr}
                            name="cr"
                          />
                          {touched.cr && errors.cr && (
                            <Text style={[allReuseStyles.error]}>
                              {errors.cr}
                            </Text>
                          )}
                        </View>
                        <View style={[allReuseStyles.inputContainer]}>
                          <Text style={[allReuseStyles.label]}>
                            Cases Booked
                          </Text>
                          <TextInput
                            style={[allReuseStyles.input]}
                            onChangeText={handleChange("casesBooked")}
                            placeholder="Cases Booked"
                            value={values.casesBooked}
                            name="casesBooked"
                            keyboardType="numeric"
                          />
                          {touched.casesBooked && errors.casesBooked && (
                            <Text style={[allReuseStyles.error]}>
                              {errors.casesBooked}
                            </Text>
                          )}
                        </View>

                        <View style={[allReuseStyles.inputContainer]}>
                          <Text style={[allReuseStyles.label]}>U/S</Text>
                          <TextInput
                            style={[allReuseStyles.input]}
                            onChangeText={handleChange}
                            placeholder="U/S"
                            value={values.us}
                            name="us"
                          />
                          {touched.us && errors.us && (
                            <Text style={[allReuseStyles.error]}>
                              {errors.us}
                            </Text>
                          )}
                        </View>
                        <View style={[allReuseStyles.inputContainer]}>
                          <Text style={[allReuseStyles.label]}>
                            Persons Arrested
                          </Text>
                          <TextInput
                            style={[allReuseStyles.input]}
                            onChangeText={handleChange("personsArrested")}
                            value={values.personsArrested}
                            placeholder="Persons Arrested"
                            keyboardType="numeric"
                            name="personsArrested"
                          />
                          {touched.personsArrested &&
                            errors.personsArrested && (
                              <Text style={[allReuseStyles.error]}>
                                {errors.personsArrested}
                              </Text>
                            )}
                        </View>
                        <View style={[allReuseStyles.inputContainer]}>
                          <Text style={[allReuseStyles.label]}>
                            Contraband Seized
                          </Text>
                          <TextInput
                            style={[allReuseStyles.input]}
                            onChangeText={handleChange("contrabandSeized")}
                            value={values.contrabandSeized}
                            placeholder="Contraband Seized"
                            name="contrabandSeized"
                            keyboardType="numeric"
                          />
                          {touched.contrabandSeized &&
                            errors.contrabandSeized && (
                              <Text style={[allReuseStyles.error]}>
                                {errors.contrabandSeized}
                              </Text>
                            )}
                        </View>
                        <View style={[allReuseStyles.inputContainer]}>
                          <Text style={[allReuseStyles.label]}>
                            Contraband Value
                          </Text>
                          <TextInput
                            style={[allReuseStyles.input]}
                            onChangeText={handleChange("contrabandValue")}
                            value={values.contrabandValue}
                            placeholder="Contraband Value in  &#8377;"
                            name="contrabandValue"
                            keyboardType="numeric"
                          />
                          {touched.contrabandValue &&
                            errors.contrabandValue && (
                              <Text style={[allReuseStyles.error]}>
                                {errors.contrabandValue}
                              </Text>
                            )}
                        </View>
                      </>
                    )}
                    <View style={[allReuseStyles.inputContainer]}>
                      <Text style={[allReuseStyles.label]}>Remarks</Text>
                      <TextInput
                        style={[allReuseStyles.input]}
                        onChangeText={handleChange("remarks")}
                        value={values.remarks}
                        placeholder="Remarks"
                        name="remarks"
                      />
                      {touched.remarks && errors.remarks && (
                        <Text style={[allReuseStyles.error]}>
                          {errors.remarks}
                        </Text>
                      )}
                    </View>
                    <ReUseButtonOne
                      onPress={resetForm}
                      text="Reset"
                      btnStyles={{
                        ...allReuseStyles.btnStyles,
                        ...allReuseStyles.submitBtn,
                        bgColor: "gray",
                        color: "white",
                      }}
                    />
                    <ReUseButtonOne
                      onPress={handleSubmit}
                      text="Submit"
                      btnStyles={{
                        ...allReuseStyles.btnStyles,
                        ...allReuseStyles.submitBtn,
                        bgColor: "gray",
                        color: "white",
                      }}
                    />
                    <ReUseButtonOne
                      onPress={handleCloseModal}
                      text="Close"
                      btnStyles={{
                        ...allReuseStyles.btnStyles,
                        ...allReuseStyles.submitBtn,
                        bgColor: "gray",
                        color: "white",
                      }}
                    />
                  </View>
                )}
              </Formik>
            </ScrollView>
          </View>
        </Modal>
      )}
    </View>
  );
};

export default ModalField;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  selectContainer: {
    padding: 16,
  },
  dropdown: {
    height: 60,
    borderColor: "gray",
    borderWidth: 0.5,
    borderRadius: 8,
    paddingHorizontal: 8,
  },
  icon: {
    marginRight: 5,
  },
  label: {
    position: "absolute",
    backgroundColor: "white",
    left: 22,
    top: 8,
    zIndex: 999,
    paddingHorizontal: 8,
    fontSize: 14,
  },
  placeholderStyle: {
    fontSize: 14,
    color: "gray",
  },
  selectedTextStyle: {
    fontSize: 16,
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },
});
