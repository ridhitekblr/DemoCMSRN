import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container:{
        flex:0,
        justifyContent:'center',
        alignItems:'center',
        paddingTop:'10%',
    },
    cont1:{
       
        width:'90%',
        justifyContent:'center',
        alignItems:'center',
        borderColor:'whitesmoke',
        borderWidth:5,
        margin:'auto',
        padding:"2%",
        borderRadius:10
    },
    imageview:{
        width:'80%',
        justifyContent:'center',
        alignItems:'center'
    },
    image:{
    width: '100%',
    aspectRatio: 3.4,
    resizeMode: 'contain', 
    },
    
    radioGroup:{
        flexDirection:'row',
        
    },
    radioButton:{
        marginHorizontal:'5%',
    },
    input: {
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    marginBottom: '6%',
    paddingHorizontal: '8%',
    paddingVertical:'3%',
    borderRadius:10,
  },
  text:{
    fontSize:18,
    fontWeight:'bold',
    paddingBottom:'3%'
  },
  touch:{
    color:'green'
  },
  t1:{
    padding:'4%',
    backgroundColor:'green',
    textAlign:'center',
    fontSize:16,
    color:'white',
    fontWeight:'bold',
    borderRadius:10,
  },
  err:{
    color:'red',
  },
  icon:{
        position:'absolute',
        top:'58%',
        left:'90%'
     },
     form:{
        justifyContent:'center',
        aspectRatio: 1.2,
    resizeMode: 'contain', 
     }
})
export default styles