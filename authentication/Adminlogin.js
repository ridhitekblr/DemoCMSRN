import { View, Text, Image,TextInput ,TouchableOpacity, StatusBar} from 'react-native'
import React from 'react'
import styles from './Adminlogincss'
import { useState } from 'react';
import Ionicons from '@expo/vector-icons/Ionicons';
import RadioButtonGroup, { RadioButtonItem } from "expo-radio-button";
  
export default function Adminlogin({navigation}) {
    const [current, setCurrent] = useState("test");  //for radiobuttons state
    const [email, setEmail] = useState('');
    const [emailVerify, setEmailVerify] = useState(false);
    const [password, setPassword] = useState('');
    const [passwordValid, setPasswordValid] = useState(false);
    const [showPassword , setShowPassword]=useState(false);
    const handleLogin = () => {                             // function for form login and validation 
      if (email === '' || password === '') {
        setEmailVerify(email === '');
        setPasswordValid(password === '');
      } else {
        switch(current) {                                  // case for radio buttons and navigation to corresponding page
          case "Agent":
            navigation.navigate('agenthome');
            break;
          case "DC":
            navigation.navigate('DCdashboard');
            break;
          case "EHO":
            navigation.navigate('EHO');
            break;
          default:
            break;
        }
      }
    };
  
    const Email = (em) => {                                    // validation for email field
      setEmail(em);
      setEmailVerify(em === '');
    };
  
    const Password = (pwd) => {                               // password field validation
      setPassword(pwd);
      setPasswordValid(pwd === '');
    };
  
  return (
    <View style={styles.container}>
    <StatusBar></StatusBar>
        <View  style={styles.cont1}>
              <View style={styles.imageview}>
              <Image source={require('../assets/logo.jpg')} style={styles.image}></Image>
              </View>
              
              <View style={styles.radioview}> 
              <RadioButtonGroup                              // container for radioitems
              containerStyle={styles.radioGroup}
              selected={current}
              onSelected={(value) => setCurrent(value)}
              radioBackground="green"
            >
           
              <RadioButtonItem value="Agent" label="Agent" style={styles.radioButton} /> 
              <RadioButtonItem value="DC" label="DC" style={styles.radioButton} />
              <RadioButtonItem value="EHO" label="EHO" style={styles.radioButton} />
              
            </RadioButtonGroup>
      
              </View>
              <View style={styles.form}>
              <Text style={styles.text}>Employee Id : </Text>
              <TextInput
                style={styles.input}
                placeholder="Employee id "
                value={email}
                onChangeText={(em)=>{
                    Email(em)
                  }}
                
                autoCapitalize="none"
              />
              {emailVerify?<Text style={styles.err}>enter valid email address</Text>:null}
              <Text style={styles.text}>Password : </Text>
              <TextInput
                style={styles.input}
                placeholder="Password"
                value={password}
                onChangeText={(em)=>{
                    Password(em)
                  }}
                  secureTextEntry={!showPassword}
                autoCapitalize="none"
              />
              <Ionicons size={20} style={styles.icon} name={ showPassword ? 'eye-off' : 'eye' } onPress={()=>{setShowPassword(!showPassword)}} />
              {passwordValid ? <Text style={styles.err}>Enter Valid Password</Text> : null}
              <TouchableOpacity style={styles.touch} onPress={handleLogin}>
              <Text style={styles.t1}>Login</Text>
              </TouchableOpacity>
             
              </View>
        </View>
        </View>
  )
}