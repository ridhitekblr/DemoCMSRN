import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
    flex: 0,
    justifyContent: 'center',
    paddingTop:'20%',
    alignItems:'center',
  },
  cont1:{
       
        width:'90%',
        justifyContent:'center',
        alignItems:'center',
        borderColor:'whitesmoke',
        borderWidth:5,
        margin:'auto',
        padding:"2%",
        borderRadius:10
    },
  imageview:{
        width:'80%',
        justifyContent:'center',
        alignItems:'center'
    },
    image:{
    width: '100%',
    aspectRatio: 3.4,
    resizeMode: 'contain', 
    },
    form:{
        justifyContent:'center',
        aspectRatio: 1.2,
    resizeMode: 'contain', 
     },
  input: {
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    marginBottom: '6%',
    paddingHorizontal: '8%',
    paddingVertical:'3%',
    borderRadius:10,
  },
  text:{
    fontSize:16,
    fontWeight:'bold',
    paddingBottom:'3%'
  },
  touch:{
    color:'green'
  },
  t1:{
    padding:'4%',
    backgroundColor:'green',
    textAlign:'center',
    fontSize:16,
    color:'white',
    fontWeight:'bold',
    borderRadius:10,
  },
  err:{
    color:'red',
    
  },
  icon:{
        position:'absolute',
        top:'57%',
        left:'88%'
     }
})
export default styles