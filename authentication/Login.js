// user Login page
import { View, Text, TextInput,Image, TouchableOpacity, StatusBar } from 'react-native'
import React from 'react'
import styles from './Logincss'
import Ionicons from '@expo/vector-icons/Ionicons';
import { useState } from 'react';
export default function Login({navigation}) {
    const [email, setEmail] = useState('');
    const [emailVerify, setEmailVerify] = useState(false);
    const [password, setPassword] = useState('');
    const [passwordValid, setPasswordValid] = useState(false);
    const [showPassword , setShowPassword]=useState(false);
    const handleLogin = () => {                       // form submit logic
        if (email === '' || password===''){
          setEmailVerify(true)
          setPasswordValid(true)
        }
        else{
          navigation.navigate('Home')
        }
      }
      const Email=(em)=>{                             // email and password validation conditions
        setEmail(em)
        if(email === ''){
          setEmailVerify(true)
        }else{
          setEmailVerify(false)
        }
      }
      const Password=(em)=>{
        setPassword(em)
        if(password === ''){
          setPasswordValid(true)
        }else{
          setPasswordValid(false)
        }
      }
    
  return (
    
      <View style={styles.container}>
      <StatusBar></StatusBar>
      <View  style={styles.cont1}>
             <View style={styles.imageview}>
              <Image source={require('../assets/logo.jpg')} style={styles.image}></Image>
              </View>
              <View style={styles.form}>
      <Text style={styles.text}>Complaint Id : </Text>
      <TextInput
        style={styles.input}
        placeholder="complaint id "
        value={email}
        onChangeText={(em)=>{
            Email(em)
          }}
        keyboardType="email-address"
        autoCapitalize="none"
      />
      {emailVerify?<Text style={styles.err}>enter valid email address</Text>:null}
      <Text style={styles.text}>Password : </Text>
      <TextInput
        style={styles.input}
        placeholder="Password"
        value={password}
        onChangeText={(em)=>{
            Password(em)
          }}
          secureTextEntry={!showPassword}
        autoCapitalize="none"
      />
      <Ionicons size={20} style={styles.icon} name={ showPassword ? 'eye-off' : 'eye' } onPress={()=>{setShowPassword(!showPassword)}} />
      {passwordValid ? <Text style={styles.err}>Enter Valid Password</Text> : null}
      <TouchableOpacity style={styles.touch} onPress={handleLogin}>
      <Text style={styles.t1}>Login</Text>
      </TouchableOpacity>
      </View>
    </View>
    </View>
      
  )
}
